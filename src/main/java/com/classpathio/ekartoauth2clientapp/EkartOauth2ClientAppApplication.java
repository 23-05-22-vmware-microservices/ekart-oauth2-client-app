package com.classpathio.ekartoauth2clientapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EkartOauth2ClientAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(EkartOauth2ClientAppApplication.class, args);
	}

}
