package com.classpathio.ekartoauth2clientapp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().disable();
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/login**", "/logout**", "/contact**")
                    .permitAll()
                .antMatchers("/api/userinfo**")
                    .authenticated()
                .and()
                .oauth2Login()
                    .redirectionEndpoint()
                .baseUri("/authorization-code/callback");
    }
}
