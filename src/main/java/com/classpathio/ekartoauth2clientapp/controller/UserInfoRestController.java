package com.classpathio.ekartoauth2clientapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/userinfo")
@RequiredArgsConstructor
public class UserInfoRestController {

    private final OAuth2AuthorizedClientService oAuth2AuthorizedClientService;
    @GetMapping
    public Map<String, String> getUserInfo(OAuth2AuthenticationToken token){

        OAuth2AuthorizedClient oAuth2AuthorizedClient = this.oAuth2AuthorizedClientService
                                                            .loadAuthorizedClient(token.getAuthorizedClientRegistrationId(), token.getPrincipal().getName());
        OAuth2AccessToken accessToken = oAuth2AuthorizedClient.getAccessToken();
        String scopes = accessToken.getScopes().toString();
        String issuedAt = accessToken.getIssuedAt().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_DATE_TIME);
        String expirtesAt = accessToken.getExpiresAt().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_DATE_TIME);
        String jwtToken = accessToken.getTokenValue();

        Map<String, String> resultMap = new LinkedHashMap<>();
        resultMap.put("token", jwtToken);
        resultMap.put("issued-at", issuedAt);
        resultMap.put("expires-at", expirtesAt);
        resultMap.put("scopes", scopes);

        return resultMap;

    }
}
